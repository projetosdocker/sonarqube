# Observacoes
- Devido a variavel ```SONARQUBE_JDBC_URL``` Foi necessario criar o container monolitico com as variaveis e depois efetuar o commit na imagem, logo em seguida subir esta imagem no harbor para ser replicada quando necessario.
- Outra observacao e que nao pode ser criado mais de um container para este servico.
- Altere o hostname do banco de dados e se precisar habilitar o LDAP, verifique as configurações.


### Preparando os diretórios de persistência de dados
```
mkdir -p /srv/deploy/sonarqube/data && \
mkdir -p /srv/deploy/sonarqube/extensions/ && \
mkdir -p /srv/deploy/sonarqube/logs/ && \
mkdir -p /srv/deploy/sonarqube/conf && \
chown -R 999:999 /srv/deploy/sonarqube
```
```
mkdir -p /srv/deploy/sonarqube/postgres/postgres && \
mkdir -p /srv/deploy/sonarqube/postgres/data
```

Crie a rede com driver bridge
```
docker network create --driver=bridge traefik-net
```

Subir o serviço.
```
docker stack deploy -c stack-postgreslocal.yml sonar
```

### Criar apenas um container de forma monolítica

```
docker run -dit \
--name ancp013 \
--hostname ANCP013 \
-p 8569:9000  \
-e SONARQUBE_JDBC_USERNAME=sonar \
-e SONARQUBE_JDBC_PASSWORD=1971zisWifRU@2013@NiprAdiTruS72014 \
-e "SONARQUBE_JDBC_URL=jdbc:mysql://anvssdf518:3306/dbsonar?useUnicode=true&characterEncoding=utf8" \
--mount type=bind,src=/opt/volumes-docker/ancp013/sonarqube/extensions,dst=/opt/sonarqube/extensions \
--mount type=bind,src=/opt/volumes-docker/ancp013/sonarqube/conf,dst=/opt/sonarqube/conf \
--mount type=bind,src=/opt/volumes-docker/ancp013/sonarqube/data,dst=/opt/sonarqube/data \
sonarqube:7.6-community
```
